package model

type DBConfig struct {
	DBName   string
	Username string
	Password string
	Host     string
	Port     string
}

type AWSConfig struct {
	URL_S3           string
	Region           string
	AccessKeyId      string
	SecreetAccessKey string
}
