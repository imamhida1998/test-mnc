package usecase

import (
	"fmt"
	"os"
	"path/filepath"
	"test-mnc/service/helpers"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/gin-gonic/gin"
)

type AwsUsecase interface {
	GetListBuckets() (*s3.ListBucketsOutput, error)
	UploadImage(c *gin.Context, formKey string, newFilename *string) (result *s3manager.UploadOutput, filename string, err error)
}

type S3AwsConstruct struct {
	sess *session.Session
	svc  *s3.S3
}

func S3AwsInit(sess *session.Session, svc *s3.S3) *S3AwsConstruct {
	return &S3AwsConstruct{
		sess: sess,
		svc:  svc, // Create S3 service client
	}
}

func (a *S3AwsConstruct) GetListBuckets() (*s3.ListBucketsOutput, error) {
	result, err := a.svc.ListBuckets(nil)
	if err != nil {
		return nil, err
	}

	fmt.Println("Buckets:")
	for _, b := range result.Buckets {
		fmt.Printf("* %s created on %s\n", aws.StringValue(b.Name), aws.TimeValue(b.CreationDate))
	}

	return result, nil
}

func (a *S3AwsConstruct) UploadImage(c *gin.Context, formKey string, newFilename *string) (result *s3manager.UploadOutput, filename string, err error) {
	file, header, err := c.Request.FormFile(formKey)
	if err != nil {
		return nil, "", err
	}
	defer file.Close()

	valid := helpers.ValidateImage(file)
	if !valid {
		return nil, "", fmt.Errorf("format image not valid, only jpeg, jpg, and png")
	}

	filename = header.Filename
	if newFilename != nil {
		//rename file IF newFilename is set
		extenstion := filepath.Ext(filename)
		filename = *newFilename + extenstion
	}

	//upload to the s3 bucket
	uploader := s3manager.NewUploader(a.sess)
	result, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(os.Getenv("AWS_S3_BUCKET_NAME")),
		Key:    aws.String(filename),
		Body:   file,
	})
	if err != nil {
		return result, filename, err
	}

	return result, filename, nil
}
