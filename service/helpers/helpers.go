package helpers

import (
	"fmt"
	"mime/multipart"
	"net/http"
	"runtime"
	"strings"
)

func ValidateImage(f multipart.File) bool {
	runtime.GOMAXPROCS(runtime.NumCPU())

	buff := make([]byte, 512)
	if _, err := f.Read(buff); err != nil {
		fmt.Println("========= level debug ==============")
		fmt.Println("user updaload image")
		fmt.Printf("message : %s\n", err.Error())
		return false
	}

	filetype := http.DetectContentType(buff)
	switch filetype {
	case "image/jpeg", "image/jpg":
		return true
	case "image/png":
		return true
	default:
		return false
	}
}

func GetTextBetween(value string, a string, b string) string {
	// Get substring between two strings.
	posFirst := strings.Index(value, a)
	if posFirst == -1 {
		return ""
	}
	posLast := strings.Index(value, b)
	if posLast == -1 {
		return ""
	}
	posFirstAdjusted := posFirst + len(a)
	if posFirstAdjusted >= posLast {
		return ""
	}
	return value[posFirstAdjusted:posLast]
}
