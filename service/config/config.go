package config

import (
	"os"
	"test-mnc/service/model"

	"github.com/joho/godotenv"
)

type Config struct {
}

func (c *Config) InitEnv() error {
	err := godotenv.Load("crud.env")
	if err != nil {
		return err
	}
	return err
}

func (c *Config) CatchError(err error) {
	if err != nil {
		panic(any(err))
	}
}

func (c *Config) GetDBConfig() model.DBConfig {
	return model.DBConfig{
		DBName:   os.Getenv("DB_NAME"),
		Username: os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PWD"),
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
	}
}

func (c *Config) AWSConfig() model.AWSConfig {
	return model.AWSConfig{
		URL_S3:           os.Getenv("S3_URL"),
		Region:           os.Getenv("S3_REGION"),
		AccessKeyId:      os.Getenv("S3_ACCESS_KEY"),
		SecreetAccessKey: os.Getenv("S3_SECRETKEY"),
	}
}
