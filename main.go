package main

import (
	"log"
	"net/http"
	"strings"
	"test-mnc/lib"
	"test-mnc/lib/db"
	"test-mnc/service/config"
	"test-mnc/service/handler"
	"test-mnc/service/repository"
	"test-mnc/service/usecase"

	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

func main() {
	route := gin.Default()
	set := config.Config{}
	set.CatchError(set.InitEnv())
	Database := set.GetDBConfig()
	aws := set.AWSConfig()
	db, err := db.ConnectiontoMYSQL(Database)
	if err != nil {
		log.Println(err)
		return
	}

	sessionAWs := lib.ConnectAws(aws)
	svc := s3.New(sessionAWs)

	UserRepo := repository.NewRepoUser(db)
	AuthUsecase := usecase.NewJWTService()
	Aws := usecase.S3AwsInit(sessionAWs, svc)
	UserUsecase := usecase.NewUsecaseUser(UserRepo, AuthUsecase, Aws)

	HanderUser := handler.NewHandlerUser(UserUsecase)

	user := route.Group("/api/user")

	user.POST("/registration", HanderUser.RegistrationDataUser)
	user.GET("/login", HanderUser.LoginDataUser)
	user.PUT("/:id", authMiddleware(AuthUsecase, UserRepo), HanderUser.UpdateDataUser)
	user.GET("/:id", authMiddleware(AuthUsecase, UserRepo), HanderUser.DetailUsers)
	user.POST("/upload", authMiddleware(AuthUsecase, UserRepo), HanderUser.UploadFotoUser)
	server := http.Server{
		Addr:    "127.0.0.1:8002",
		Handler: route,
	}
	server.ListenAndServe()
	if err != nil {
		return
	}

}

func authMiddleware(authService usecase.Auth, userService repository.UserRepo) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if !strings.Contains(authHeader, "Bearer") {
			c.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
			return
		}

		var tokenString string
		arrayToken := strings.Split(authHeader, " ")
		if len(arrayToken) == 2 {
			tokenString = arrayToken[1]
		}

		token, err := authService.ValidateToken(tokenString)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
			return
		}
		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			c.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
			return
		}

		Email := (claim["email"].(string))

		user, err := userService.GetUsersByEmail(Email)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized")
			return
		}

		c.Set("CurrentUser", user)

	}
}
